#!/usr/bin/php
<?php

/***
 * This file was created with the intention of deploying a web browser view index. To be completed.
 ***/

require_once("classes/IO.class.php");
require_once("classes/Validation.class.php");
require_once("classes/Action.class.php");
require_once("classes/GetData.class.php");
require_once("classes/GenericExec.class.php");

// Presentation of the program
IO::outPresentation();

// TODO: Uncomment this line to execute from cmd manual input. unfinished.
// GenericExec::execViaCLI($argv);

// TODO: Uncomment these line to execute from txt file input absolute path
$file = '/home/vagrant/Code/toyrobotsimulatorsimple/storage/data.txt';
GenericExec::execViaTextFile($file);

?>
