<?php

class GenericExec
{

    /**
     * Execute the program based on text file input
     * @param $file
     */
    public static function execViaTextFile($file)
    {
        $storage = ['currCoordinates' => null, 'currFace' => null, 'hasBeenPlaced' => false];
        $values = ['coordinates' => false, 'face' => false, 'action' => null];
        $fh = fopen($file, 'r');
        while ($line = fgets($fh)) {
            $line = str_replace(array("\n", "\t", "\r"), '', $line);

            $values['coordinates'] = false;
            $values['face'] = false;
            $values['action'] = Validation::isValidAction(explode(" ", $line)[0]);

            list($values, $storage) = Action::act($values, $line, $storage);

            $storage = Validation::tryMove($values, $storage);

        }
        fclose($fh);
    }


    /**
     * Execute the program based on direct CLI command line
     */
    public static function execViaCLI($argv)
    {
        $cmd = IO::getCommand($argv);
        $values['coordinates'] = [Validation::isValidCoordinate(GetData::getLocation($cmd)[0]), Validation::isValidCoordinate(GetData::getLocation($cmd)[1])];
        $values['face'] = Validation::isValidFace(GetData::getLocation($cmd)[2]);
        // Execution of the program
        $cmd = IO::getCommand($argv);
    }
}
