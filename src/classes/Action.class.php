<?php

class Action
{

    /**
     * Set the move of the robot
     * @param $storage
     * @param $values
     * @return mixed
     */
    public static function move($storage, $values)
    {
        switch ($storage ['currFace']) {
            case "NORTH":
                $values['coordinates'] = [Validation::isValidCoordinate($storage ['currCoordinates'][0]), Validation::isValidCoordinate($storage ['currCoordinates'][1] + 1)];
                break;
            case "SOUTH":
                $values['coordinates'] = [Validation::isValidCoordinate($storage ['currCoordinates'][0]), Validation::isValidCoordinate($storage ['currCoordinates'][1] - 1)];
                break;
            case "EAST":
                $values['coordinates'] = [Validation::isValidCoordinate($storage ['currCoordinates'][0] + 1), Validation::isValidCoordinate($storage ['currCoordinates'][1])];
                break;
            case "WEST":
                $values['coordinates'] = [Validation::isValidCoordinate($storage ['currCoordinates'][0] - 1), Validation::isValidCoordinate($storage ['currCoordinates'][1])];
                break;
        }
        $values['face'] = $storage ['currFace'];
        return $values;
    }


    /**
     * Turn left as required
     * @param $storage
     * @param $values
     * @return array
     */
    public static function left($storage, $values)
    {
        $values['coordinates'] = $storage ['currCoordinates'];
        switch ($storage ['currFace']) {
            case "NORTH":
                if ($values['action'] === "LEFT") {
                    $storage ['currFace'] = "WEST";
                } else {
                    $storage ['currFace'] = "EAST";
                }
                break;
            case "SOUTH":
                if ($values['action'] === "LEFT") {
                    $storage ['currFace'] = "EAST";
                } else {
                    $storage ['currFace'] = "WEST";
                }
                break;
            case "EAST":
                if ($values['action'] === "LEFT") {
                    $storage ['currFace'] = "NORTH";
                } else {
                    $storage ['currFace'] = "SOUTH";
                }
                break;
            case "WEST":
                if ($values['action'] === "LEFT") {
                    $storage ['currFace'] = "SOUTH";
                } else {
                    $storage ['currFace'] = "NORTH";
                }
                break;
        }
        return array($values, $storage);
    }


    /**
     * Execute the reporting
     * @param $storage
     */
    public static function report($storage)
    {
        IO::getReport(['face' => $storage ['currFace'], 'coordinates' => $storage ['currCoordinates']]);
    }


    /**
     * Execute the placing of the robot
     * @param $line
     * @param $values
     * @return mixed
     */
    public static function place($line, $values)
    {
        $placeArgs = explode(",", explode(" ", $line)[1]);
        $values['coordinates'] = [Validation::isValidCoordinate($placeArgs[0]), Validation::isValidCoordinate($placeArgs[1])];
        $values['face'] = Validation::isValidFace($placeArgs[2]);
        return $values;
    }


    /**
     * Action in motion
     * @param $values
     * @param $line
     * @param $storage
     * @return array
     */
    public static function act($values, $line, $storage)
    {
        if ($values['action'] === "PLACE") {
            $values = Action::place($line, $values);
        }

        if ($values['action'] === "MOVE" && $storage ['hasBeenPlaced']) {
            $values = Action::move($storage, $values);
        }

        if (($values['action'] === "LEFT" || $values['action'] === "RIGHT") && $storage ['hasBeenPlaced']) {
            list($values, $storage) = Action::left($storage, $values);
        }

        if ($values['action'] === "REPORT" && $storage ['hasBeenPlaced']) {
            Action::report($storage);
        }
        return array($values, $storage);
    }

}

?>