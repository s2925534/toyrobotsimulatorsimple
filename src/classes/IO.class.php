<?php

class IO
{

    /**
     * Command line initial presentation
     */
    public static function outPresentation()
    {
        IO::outBoardView();
        IO::getPause();
        IO::outInstructions();
        IO::getPause();
    }


    /**
     * View of the board
     */
    public static function outBoardView()
    {
        echo "
        **************************************************\n
        ***************TOY-ROBOT-SIMULATOR****************\n
        **************************************************\n
        ******************** NORTH ***********************\n
        **************************************************\n
        *****************40*41*42*43*44*******************\n
        *****************30*31*32*33*34*******************\n
        ****** WEST *****20*21*22*23*24***** EAST ********\n
        *****************10*11*12*13*14*******************\n
        *****************00*01*02*03*04*******************\n
        **************************************************\n
        ******************** SOUTH ***********************\n
        **************************************************\n
        **************************************************\n
        *****************  *  *  *  *  *******************\n
        *****************  *  *  *  *  *******************\n
        *****************  *  *  *  *  *******************\n
        *****************  *  *  *  *  *******************\n
        *****************  *  *  *  *  *******************\n
        **************************************************\n
        **************************************************\n";
    }


    /**
     * Just instructions
     * @param bool $pause
     */
    public static function outInstructions($pause = false)
    {
        echo "
        ************ INSTRUCTIONS ************ \n
        Description:\n
         - The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units. \n
        - There are no other obstructions on the table surface. - The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement that would result in the robot falling from the table must be prevented, however further valid movement commands must still be allowed.\n
        Create an application that can read in commands of the following form – PLACE X,Y,F MOVE LEFT RIGHT REPORT\n
        - PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST. \n
        - The origin (0,0) can be considered to be the SOUTH WEST most corner. \n
        - The first valid command to the robot is a PLACE command, after that, any sequence of commands may be issued, in any order, including another PLACE command. The application should discard all commands in the sequence until a valid PLACE command has been executed. \n
        - MOVE will move the toy robot one unit forward in the direction it is currently facing. \n
        - LEFT and RIGHT will rotate the robot 90 degrees in the specified direction without changing the position of the robot. \n";

        if($pause){
            IO::getPause();
        }

        echo "- REPORT will announce the X,Y and F of the robot. This can be in any form, but standard output is sufficient.\n
        - A robot that is not on the table can choose the ignore the MOVE, LEFT, RIGHT and REPORT commands. \n
        - Input can be from a file, or from standard input, as the developer chooses. \n
        - Provide test data to exercise the application.\n
        Constraints: The toy robot must not fall off the table during movement. This also includes the initial placement of the toy robot. Any move that would cause the robot to fall must be ignored.\n
        Example Input and Output: \n
        a) PLACE 0,0,NORTH\nMOVE \nREPORT \nOutput: 0,1,NORTH\n
        b) PLACE 0,0,NORTH \nLEFT \nREPORT \nOutput: 0,0,WEST\n
        c) PLACE 1,2,EAST \nMOVE \nMOVE \nLEFT \nMOVE \nREPORT \nOutput: 3,3,NORTH\n
        - There must be a way to supply the application with input data via text file.\n
        - The application must run and you should provide sufficient evidence that your solution is complete by, as a minimum, indicating that it works correctly against the supplied test data.\n
        - The submission should be production quality PHP code. We are not looking for a gold plated solution, but the code should be maintainable and extensible.\n
        - You may not use any external libraries to solve this problem, but you may use external libraries or tools for building or testing purposes. Specifically, you may use unit testing libraries or build tools.\n
        - You should provide a readme file, detailing installation and execution instruction as well as a brief summary of assumptions and design decisions made.\n
- The submission should be provided via GitHub, Bitbucket or some other online version control system\n
        **************************************************\n";
    }

    /**
     * Pause until next key is pressed
     */
    public static function getPause()
    {
        self::outLine("Press enter to continue\n\n");
        $fp = fopen("php://stdin", "r");
        fgets($fp);
    }

    /**
     * Output on monitor whatever is given
     * @param $str
     * @return bool
     */
    public static function outLine($str)
    {
        echo $str . "\n";
        return true;
    }

    /**
     * Call command line input
     * @return array
     */
    public static function inLine()
    {
        return explode(' ', trim(fgets(STDIN, 1024)));
    }

    /**
     * Execute command line input of arguments
     * @param $argv
     * @return array|string
     */
    public static function getCommand($argv)
    {
        return Validation::getValidation($argv);
    }


    /**
     * Report action generator
     * @param $criteria
     * @return bool
     */
    public static function getReport($criteria)
    {
        return IO::outLine("Robot is facing " . $criteria['face'] . " in coordinates X:" . $criteria['coordinates'][0] . ", Y:" . $criteria['coordinates'][1]);
    }

}
