<?php

class GetData
{

    /**
     * Resolve data from CLI input
     * @param $cmd
     * @return array|string
     */
    public static function getValues($cmd)
    {
        if (sizeof($cmd) < 3) return "Invalid input. Please try again.";

        $action = Validation::isValidAction($cmd[1]);
        $coordinates = [Validation::isValidCoordinate(self::getLocation($cmd)[0]), Validation::isValidCoordinate(self::getLocation($cmd,',')[1])];
        $face = Validation::isValidFace(self::getLocation($cmd,',')[2]);

        if ($action && $coordinates && $face && $cmd[3] )
            return ['action' => $action, 'coordinates' => $coordinates, 'face' => $face];
        else
            return null;
    }


    /**
     * Get location from string based on separator
     * @param $cmd
     * @param string $separator
     * @return array
     */
    public static function getLocation($cmd, $separator = " ")
    {
        return explode($separator, $cmd[2]);
    }

}

?>