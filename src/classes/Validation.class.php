<?php

class Validation extends IO
{

    /**
     * Execute validation
     * @param $cmd
     * @return array|string
     */
    public static function getValidation($cmd)
    {
        return GetData::getValues($cmd);
    }

    /**
     * Validate actions
     * @param $str
     * @return bool
     */
    public static function isValidAction($str)
    {
        switch ($str) {
            case "PLACE":
            case "MOVE":
            case "LEFT":
            case "RIGHT":
            case "REPORT":
                return $str;
                break;
            default:
                self::reportException($str, "MOVE, LEFT, RIGHT, or REPORT.");
                return false;
        }
    }

    /**
     * Validate coordinates X or Y
     * @param $xORy
     * @return bool
     */
    public static function isValidCoordinate($xORy)
    {
        switch ($xORy) {
            case $xORy == 0:
            case $xORy == 1:
            case $xORy == 2:
            case $xORy == 3:
            case $xORy == 4:
                return $xORy;
            default:
                self::reportException($xORy, "MOVE, LEFT, RIGHT, or REPORT.");
                return false;
        }
    }


    /**
     * Validates the facing direction of the robot
     * @param $str
     * @return bool
     */
    public static function isValidFace($str)
    {
        switch ($str) {
            case "NORTH":
            case "SOUTH":
            case "WEST":
            case "EAST":
                return $str;
            default:
                self::outLine($str . " is NOT a valid orientation for our poor robot! Please try using NORTH, SOUTH, EAST or WEST.");
                return false;

        }
    }


    /**
     * Report exception as required
     * @param $str
     */
    public static function reportException($str, $tip)
    {
        self::outLine($str . " is NOT a valid action!");
        self::outLine(" Please try using " . $tip);
    }



    /**
     * Try to move while lookingh for error
     * @param $values
     * @param $storage
     * @return mixed
     */
    public static function tryMove($values, $storage)
    {
        if ($values['coordinates'] && $values['face']) {   // attempt to move
            $storage ['currCoordinates'] = $values['coordinates'];
            $storage ['currFace'] = $values['face'];
            if ($values['action'] === "PLACE") {
                $storage ['hasBeenPlaced'] = true;
            }
        }
        return $storage;
    }
}
